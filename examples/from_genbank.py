from dna_features_viewer import BiopythonTranslator
from Bio.SeqRecord      import SeqRecord

class SarsCov2Translator(BiopythonTranslator):
    """Custom translator implementing the following theme:

    - Color terminators in green, CDS in blue, all other features in gold.
    - Do not display features that are restriction sites unless they are BamHI
    - Do not display labels for restriction sites.
    - For CDS labels just write "CDS here" instead of the name of the gene.
    """

    def compute_feature_color(self, feature):
        if feature.type == "CDS":
            return "blue"
        elif feature.type == "mat_peptide":
            return "green"
        else:
            return "gold"

    def compute_feature_label(self, feature):
        if feature.type == 'restriction_site':
            return None
        elif feature.type == "CDS":
            return "CDS"
        elif feature.type == "mat_peptide":
            return "m_peptide"
        else:
            return BiopythonTranslator.compute_feature_label(self, feature)

    def compute_filtered_features(self, features):
        """Do not display promoters. Just because."""
        return [
            feature for feature in features
            if (feature.type != "restriction_site")
            or ("BamHI" in str(feature.qualifiers.get("label", '')))
        ]

# Exemple de la documentació.
# genbank_file: str = "example_sequence.gb"
GENBANK_FILE: str = "sars-cov-2.genbank"

graphic_record = SarsCov2Translator().translate_record(GENBANK_FILE)
ax, _ = graphic_record.plot(figure_width=10, strand_in_label_threshold=7)
ax.figure.tight_layout()
ax.figure.savefig("sars-cov-2_genbank.png")
# ax.figure.savefig("from_genbank.png")
