# Exemples del projecte DNA Features Viewer

El projecte **DNA Features Viewer** és una llibreria de Python que permet visualitzar clarament carecterístiques de cadenes d'ADN a partir de fitxers GenBank, GFF o SeqRecors de Biopython. 

Per sota utilitza llibreries gràfiques com Matplotlib o Bokeh (pendent provar).

Ara per ara pràcticament tot el codi és del projecte original, només hem provat de posar altres genbank o canvis estètics.

### Pàgina oficial del projecte

https://github.com/Edinburgh-Genome-Foundry/

## Guia d'ús.

Si et trobes a Linux pots executar directament aquest script:

```
./init.sh
```

En qualsevol cas, recomanem insta·lar pip i executar:

```
pip install dna_features_viewer

```

## Exemples interessants provats.

[DnaFeaturesViewer/examples/sequence_and_translation.py]

Et pinta les features que vols en els intèrvals de base que vols (per exemple, cds start - cds end)

![overview_and_detail](examples/overview_and_detail.png)

[sequence_and_translation.py](./examples/sequence_and_translation.py)

Et pinta pinta totes les features que estan documentades en el fitxer Genbank. 
Recorda que els Genbanks estan dividits en 3 parts: header, features i sequence.
Doncs amb aquest codi pots obtenir **informació del bloc més interessant pels biòlegs (cds, pèptids ...)**

[from_genbank.py](./examples/from_genbank.py)

![from_genbank](examples/from_genbank.png)

En alguns Genbank que diverses features tenen la mateixa etiqueta dins del genbank, cal personalitzar-les.
Mirem com es fa des de la classe: 

[custom_biopython_translator.py](./examples/custom_biopython_translator.py)

![custom_biopython_translator](examples/custom_biopython_translator.png)


