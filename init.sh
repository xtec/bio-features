#!/bin/bash

if [ ! -f ".venv/bin/pip" ]; then
    sudo apt update
    sudo apt install -y python3-venv
    python3 -m venv .venv
fi

./.venv/bin/pip install dna_features_viewer

# Opcional, Bokeh per animar algún gràfic.
# pip install bokeh pandas